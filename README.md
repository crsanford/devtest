# DevTest

Assignment for Repsonsive.AI Junior Developer position.

Given the size / granularity of this project, I limited changes to the directory structure. In a larger system, CSS, .html, angular modules & controllers would all be separate in their own files. I would create a new app/ folder in public/.

RESULT:
* Single page (ugly html) implementing drop down email menu & input fields for user model

* Two endpoints for retrieving user info

ENVIRONMENT:

* Ubuntu

* Chrome: 57.0.2987.133

* Node: v7.1.0

## Execution

Run server: 'node server.js'

Endpoints:

* 'http://localhost:8080/API/users/emails' : Returns list of all user emails

* 'http://localhost:8080/API/users/[email@address]' : Returns full information for one user

## Workflow

If part of a larger repo, I would have made a new branch, made changes, then made a pull request for review. However, as requested for this assignment, I just cloned, changed, and pushed direct to master.

## Approach

1) Setup local dev env

2) Added 'hello world' page + angular controller. Verified first BE endpoint + routes. 

3) Used user.json file directly to build front end requirements

4) Implemened BE async file reader & user middleware + endpoints

5) Modified FE to hit two endpoints

6) Refactor & Review

## Future

1) Use the other endpoints available in core.svc.js and post user input form data.

2) Purge $scope and use 'controllerAs' format
