'use strict';

angular.module('Core', [])

.factory('CoreSvc',  ['$http', function($http) {
    var svc = {};

    svc.HTTPGet = function(URL, callback) {
        $http.get(URL).
            success(function (data, status, headers, config) {

            if (data === null) {
                callback(null);
            }
            else if (data.errmsg) {
                console.log('Error in HTTPGet (' + URL + '):');
                console.log(data.errmsg);
                callback(null);
            }
            else {
                callback(data);
            }

        }).
        error(function (data, status, headers, config) {
            // handle entry exists
            // handle other problems
            console.log('error in ' + URL);
            console.log(data);
            console.log(status);
            console.log(headers);
            console.log(config);

            if (status === 401) console.log("unauthorized");

            callback(null);
        });
    };

    svc.HTTPPost = function(URL, inputdata, callback) {
        $http.post(URL, inputdata).
        success(function (data, status, headers, config) {
            if (data.errmsg) {
                console.log('error: ' + data.errmsg);
                callback(null);
            }
            else {
                callback(data);
            }

        }).
        error(function (data, status, headers, config) {
            //handle entry exists
            //handle other probems
            console.log('error in ' + URL);
            console.log(data);
            console.log(status);
            console.log(headers);
            console.log(config);

            if ( status === 401) window.location='/signin';

            callback(null);
        });
    };

    svc.HTTPPostForm = function(URL, params, callback) {
        $http({
            method: 'POST',
            url: URL,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            params: params
        }).success(function (result) {
            callback(result);
        }).
        error(function (data, status, headers, config) {
            console.log('error posting form');
            console.log(data);
            console.log(status);
            console.log(headers);
            console.log(config);
            callback(null);
        });
    };

    return svc;
}])
