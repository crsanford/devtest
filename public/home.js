'use strict';

angular
    .module('devTestApp', ['Core'])
    .controller('mainController', mainController);

function mainController($scope, $http, CoreSvc) {

    activate($scope, CoreSvc);

    // $scope functions are directly accessible by the view

    // Requests complete data for single user
    $scope.handleEmailSelect = function(email) {
        let url = 'http://localhost:8080/API/users/' + email;

        CoreSvc.HTTPGet(url, function(data) {
            // Modify view as soon as data returns
            $scope.userModel = buildModel(email, data);
        });
    }

    // Adds a new key with null value to details obj
    $scope.addDetail = function() {
        let index = $scope.userModel.detailKeys.length + 1;

        $scope.userModel.detailKeys.push('someField' + index);
    }

    // Removes key and object from user details
    $scope.removeDetail = function() {
        if ($scope.userModel.detailKeys.length == 0 ) return;

        let index = $scope.userModel.detailKeys.length - 1;
        let lastKey = Object.keys($scope.userModel.details)[index];

        $scope.userModel.detailKeys.splice(index);
        delete $scope.userModel.details[lastKey];
    }
}



function buildModel(email, data) {
    return  {
        email: email || null,
        firstName: data.firstName || null,
        lastName: data.lastName || null,
        username: data.username || null,
        details: data.details || { details:null },
        detailKeys: Object.keys(data.details) || []
    }
}

// Returns list of emails available
function getUserEmails($scope, CoreSvc) {
    let url = 'http://localhost:8080/API/users/emails';
    CoreSvc.HTTPGet(url, function(data) {
        $scope.emailOptions = data;
    });
}


// Initializes empty values for the view
function activate($scope, CoreSvc) {
    let userModel = {
        email: null,
        firstName: null,
        lastName: null,
        username: null,
        details: { content:null },
        detailKeys: []
    }

    let emailOptions = null;

    $scope.userModel = userModel;
    $scope.emailOptions = emailOptions;

    // Async call to populate dropdown menu
    getUserEmails($scope, CoreSvc);
}
