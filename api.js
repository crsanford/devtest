'use strict';

var express = require('express');
var fs = require('fs');

var router = express.Router();

module.exports = function() {

    // Wide endpoint to get emails for ALL users
    // blocks until file has been read
    router.get('/users/emails', function(req, res, next) {
        readUserFile('users.json').then((data) => {
            req.data = Object.keys(data)
            next();
        });
    }, function(req, res) {
        res.setHeader('Content-Type', 'application/json');
        res.send(req.data);
    });

    // Narrow endpoint to get full data for ONE user
    // blocks until file has been read
    router.get('/users/:userEmail', function(req, res, next) {
        readUserFile('users.json').then((data) => {
            req.userData = data[req.params.userEmail];
            next();
        });
    }, function(req, res) {
        res.setHeader('Content-Type', 'application/json');
        res.send(req.userData);
    });

    return router;
};

// Creates promise; Reads user data file asynchronously
function readUserFile(fileName) {
    let dataPromise = new Promise((resolve, reject) => {
        fs.readFile(fileName, 'utf-8', (err, data) => {
            if (err) throw err;
            resolve(JSON.parse(data));
        })
    });

    return dataPromise
}
