'use strict';

var express = require('express');
var app = express();

// Limit FE (Angular) to only have access to public files
app.use(express.static(__dirname + '/public'));

// get current directory
var path = require('path');

app.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

// define API / BE routes
var api = require('./api.js')();
app.use('/API', api);

module.exports = app;
